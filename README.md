# parallelism

openmp basics

compilation sans openmp
```
g++ main.cpp
time ./a.out
```
compilation avec openmp
```
g++ -fopenmp main.cpp -o omp
time ./omp
```

--------------------------------------------------------

example de concurence:

lire un fichier xyz et faire la sum des coordonnes du point et ajouter la sum a un vecteur
```
g++ -fopenmp concurence.cpp -o concurence
./concurence
```

---------------------------------------------------------

optimisation de glcm & haralick feature
```
mkdir build && cd build
cmake ..
make
time ./omp-glcm 5 ../data/5.png > omp-haralick.csv
time ./glcm 5 ../data/5.png > haralick.csv
diff haralick.csv omp-haralick.csv
```
un gain d'environ 35% mais il y a des errors au niveau des points a virgule
a date aucune optimisation n'est faites pour la creation de la matrice glcm
