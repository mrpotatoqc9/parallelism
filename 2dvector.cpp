#include <iostream>
#include <random>
#include <bits/stdc++.h>
#include <omp.h>

bool is_prime(int n){
	int d = n-1;
	while(n%d != 0){
		d=d-1;
		if(d == 1){
			return true;
		}
	}
	return false;
}

int main(int argc, char **argv)
{	
	std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(100000,1000000000);
    
    std::vector<std::vector<int>> numbers;
    std::vector<int> row;
    
    for(int i =0; i< 10; ++i){
    	for (int j =0; j<10; ++j){
    		row.push_back(dist(rng));
    	}
    	numbers.push_back(row);
    	row.clear();    	
    }
    
    std::vector<int> ompV;
	int x =1;
	//la loop exterieur est la plus rapide a parralleliser meme tenter de parralleliser les 2 loop est plus lent
	#pragma omp parallel for 
	for(auto &vector:numbers){
		//#pragma omp parallel for
		for(auto &i : vector){
			if(is_prime(i)){
				ompV.push_back(i);
			}
			else{
				ompV.push_back(x);
				++x;
			}
		}
	}
	
	std::vector<int> normalV;
	x = 1;
	for(auto &vector:numbers){
		for(auto &i : vector){
			if(is_prime(i)){
				normalV.push_back(i);
			}
			else{
				normalV.push_back(x);
				++x;
			}
		}
	}
	
	std::sort(ompV.begin(), ompV.end());
	std::sort(normalV.begin(), normalV.end());
	int eq = 0;
	for(int i = 0; i<normalV.size(); ++i){
		if(normalV[i] == ompV[i]){
			++eq;
		}
	}
	if(eq == normalV.size()){
		std::cout<<"equal\n";
	}
	else{
		std::cout<<eq<<"\n";
	}
	
	return 0;
}
