#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>


int matrixBuilder(std::string filename, std::map<int,std::map<int,std::vector<double>>> &haralickMatrix){

	std::cout<<"building matrix for : "<<filename<<"\n";
	std::string line;
	int x,y;
	double a,b,c,d,e,f,g;
	int nLines = 0; 
	
	std::vector<double> feature;
	std::map<int,std::vector<double>> col;
	
	std::ifstream myfile (filename);
	if (myfile.is_open()){
		while ( getline (myfile,line) ) {
			std::istringstream ss(line);
			ss >> x >> y >> a >> b >> c >> d >> e >> f >> g;
			//std::cout<<x << y<< a<< b<< c<< d<< e<< g<<"\n";
			std::vector<double> feature{a,b,c,d,e,f,g};
			col.insert({x,feature});
			haralickMatrix.insert({y, col});
			++nLines;
		}
		
	}
	
	else std::cout << "Unable to open file";
	
	myfile.close();
	
	return nLines;
}

void find_errors(std::map<int,std::map<int,std::vector<double>>> haralickMatrix1, 
				std::map<int,std::map<int,std::vector<double>>> haralickMatrix2, std::vector<std::vector<int>> &errors){
	std::cout<<"searching errors \n";
	std::cout<<haralickMatrix1.size()<<"\n";
	std::cout<<haralickMatrix2.size()<<"\n";
	for(int row = 0; row<haralickMatrix1.size(); ++row){
	
		auto search = haralickMatrix1.find(row);
		std::map<int,std::vector<double>> cols = search->second;
		
		for(int col = 0; col<cols.size(); ++col){
		
			auto search2 = cols.find(col);
			std::vector<double> features1 = search2->second;
			
			auto searchMat2_row = haralickMatrix2.find(row);
			if(searchMat2_row == haralickMatrix2.end()){
				std::cout<<"one row not found \n";
				std::vector<int> error{row, -1, -1};
				errors.push_back(error);
				//break;
			}
			
			std::map<int,std::vector<double>> cols2 = searchMat2_row->second;
			
			auto searchMat2_col = cols2.find(col);
			if(searchMat2_col == cols2.end()){
				std::cout<<"one col not found \n";
				std::vector<int> error{row, col, -1};
				errors.push_back(error);
			}
			
			std::vector<double> features2 = searchMat2_col->second;
			
			for(int i = 0; i<features1.size(); ++i){
				if(features1[i] != features2[i]){
					std::vector<int> error{row, col, i};
					errors.push_back(error);
				}
			}
			
		}
	}



}


int main(int argc, char **argv){

if (argc < 3) {
		std::cerr << "usage: ./validator haralickFile haralickFile2\n";
		return -1;
	}

	
	std::string inputFile1 = argv[1];
	
	std::string inputFile2 = argv[2];
	
	std::cout<<"input files: " << inputFile1 << " && " << inputFile2 << "\n\n";
	
	std::map<int,std::map<int,std::vector<double>>> matrix1;
	std::map<int,std::map<int,std::vector<double>>> matrix2;
	
	int nLine1 = matrixBuilder(inputFile1, matrix1);
	int nLine2 = matrixBuilder(inputFile2, matrix2);
	
	std::vector<std::vector<int>> errors;
	
	if(nLine1 != nLine2){
		std::cout<<"number of lines not equal \n";
		std::cout<<nLine1<<"\n"<<nLine2<<"\n\n";
		find_errors(matrix1, matrix2, errors);
	}
	else{
		std::cerr<<"same number of lines \n";
	}
	
	if(errors.size() > 0){
		std::cout<<"row , col , feature error position \n\n";
		for(auto &error : errors){
			std::cout<<error[0]<<error[1]<<error[2]<<"\n";
		}
	}
	else{
		std::cerr<<"all features are equal \n";
	}
	
	return 0;


}
