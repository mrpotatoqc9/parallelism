#include <omp.h>
#include <iostream>
#include <vector>
#include <chrono>


bool is_prime(int n){
	int d = n-1;
	while(n%d != 0){
		d=d-1;
		if(d == 1){
			return true;
		}
	}
	return false;
}


int main(int argc, char **argv)

{	
std::vector<long unsigned int>numbers{10147258369,10159357456,10321654987,10987456321,10417528639,10965321469,10789456123,11245369852,12789456321,2147483647};
/*
	#pragma omp parallel for	
		for( auto &n : numbers){

			//std::cout<<"thread: "<< omp_get_thread_num() << "\n";
			if(is_prime(n)){
				std::cout<<"prime \n";
			}
		
			else{
				std::cout<<"not prime \n";
			}
		}

*/
	#pragma omp parallel for
	for(int i = 0; i < numbers.size(); ++i){
		std::cout<<i<<" , ";
		if(is_prime(numbers[i])){
			std::cout<<i<<"->prime";
		}
		else{
			std::cout<<i<< "->not prime";
		}
		std::cout<<"\n";
	}
	
	
	
	return 0;
}

