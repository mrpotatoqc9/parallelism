#include <iostream>
#include "opencv2/opencv.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <math.h>
#include <fstream>
#include <omp.h>
#include <algorithm>

void computeGlcm(cv::Mat &img, int windowSize, std::vector<std::vector<std::vector<double>>> &processedFeatures){
        
        	if(windowSize % 2 == 0){
        		std::cerr<<"windowSize must be odd !\n";
        		return;
        	}
        	else{
        		
        		std::vector<double> features;
        		std::vector<std::vector<double>> ys;
        		features.reserve(9);
        		ys.reserve(img.rows);
        		
        		
        		
		    	//for every window in image
		    	for(int col = windowSize/2; col<img.cols - (windowSize/2); col=col+(windowSize/2)){
		    		for (int row = windowSize/2; row<img.rows - (windowSize/2); row=row+(windowSize/2)){
		    			//compute glcm over window
		    			cv::Mat glcm(cv::Size(256,256), CV_64F, cv::Scalar(0)); // XXX
		    			int count = 0;
		    			
		    			
		    			//for pixel in window
		    			for(int windowCol = col-(windowSize/2) +1 ; windowCol<col+(windowSize/2) -1; windowCol++){
		    				for(int windowRow = row-(windowSize/2) +1 ; windowRow<row+(windowSize/2) -1; windowRow++){

		    					int value = img.at<uchar>(windowRow, windowCol, 0);
		    					int valueUp = img.at<uchar>(windowRow-1, windowCol, 0);
		    					int valueDown = img.at<uchar>(windowRow+1, windowCol, 0);
		    					int valueLeft = img.at<uchar>(windowRow, windowCol-1, 0);
		    					int valueRight = img.at<uchar>(windowRow, windowCol+1, 0);
		    					
		    					glcm.at<double>(valueUp, value, 0)++;
		    					glcm.at<double>(valueDown, value, 0)++;
		    					glcm.at<double>(valueLeft, value, 0)++;
		    					glcm.at<double>(valueRight, value, 0)++;
		    					count += 4;
		    				}
		    			}
		    			
		    			cv::Mat transposedGlcm(cv::Size(256,256), CV_64F, cv::Scalar(0));
		    			cv::transpose(glcm, transposedGlcm);
		    			cv::Mat normalizedGlcm = (glcm + transposedGlcm)/(count*2);
		    			
		    			//double min;
		    			//double max;
		    			
		    			//cv::minMaxIdx(normalizedGlcm, &min, &max);
		    			//std::cerr<<min <<" "<< max<<"\n";
		    			
		    			
		    			double glcmMean = 0;
		    			double energy = 0;
		    			double contrast = 0;
		    			double homogeneity = 0;
		    			double entropy = 0;
		    			double correlation = 0;
		    			double shade = 0;
		    			double prominence = 0;
		    			double sigma = 0;
		    			double squaredVarianceIntensity = 0;
		    			double A = 0;
		    			double B = 0;
		    			//int counting = 0;
		    			for(int c = 0; c <normalizedGlcm.cols; c++){
		    				#pragma omp parallel for reduction(+:homogeneity,energy,contrast,entropy,glcmMean)
		    				for(int r = 0; r<normalizedGlcm.rows; r++){
								//counting++;
		    					double pij = normalizedGlcm.at<double>(r,c,0);
		    					double intensity = (double)img.at<uchar>(col,row,0);
		    					
		    					if(pij != 0){
		    						//std::cerr<<pij*intensity<<"\n";
									homogeneity += pij/(1.0+((c-r)*(c-r)));
									energy += pij * pij;
									contrast += (c-r)*(c-r)*pij;
									entropy += -(log(pij)*pij); // pij will never be under 0
									glcmMean += pij * intensity;

								}
		    				}
		    			}
		    			//std::cerr<<counting<<"\n";
		    			
		    			for(int c = 0; c <normalizedGlcm.cols; c++){
		    				#pragma omp parallel for reduction(+:squaredVarianceIntensity)
		    				for(int r = 0; r<normalizedGlcm.rows; r++){

		    					double pij = normalizedGlcm.at<double>(r,c,0);
		    					double intensity = (double)img.at<uchar>(col,row,0);
		    					if(pij != 0){
		    						squaredVarianceIntensity += pij*((intensity - glcmMean)*(intensity - glcmMean));
								}
							}
						}
		    			
						
		    			for(int c = 0; c <normalizedGlcm.cols; c++){
		    				#pragma omp parallel for reduction(+:correlation)
		    				for(int r = 0; r<normalizedGlcm.rows; r++){
		    					
		    					double pij = normalizedGlcm.at<double>(r,c,0);
		    					if(pij != 0){
		    						
		    						//XXX handle very small squaredVarianceIntensity
		    						if(squaredVarianceIntensity < 1.0e-15){
		    							squaredVarianceIntensity = 1.0e-15;
		    						}
		    						
		    						
									correlation += pij*(((c-glcmMean)*(r-glcmMean))/squaredVarianceIntensity);

								}
							}
						}
						
						for(int c = 0; c <normalizedGlcm.cols; c++){
							#pragma omp parallel for reduction(+:shade,prominence)
		    				for(int r = 0; r<normalizedGlcm.rows; r++){
		    					
		    					double pij = normalizedGlcm.at<double>(r,c,0);
		    					if(pij != 0){
		    						double intensity = (double)img.at<uchar>(col,row,0);
									sigma = sqrt(squaredVarianceIntensity);
										
									A=(pow((col+row)-2.0*glcmMean,3)*pij)/(pow(sigma, 3)*(sqrt(2.0*(1+correlation))));	
									if(A > 0){
										shade += pow(abs(A), 1.0/3);
									}
									else if(A<0){
										shade += pow(abs(A), 1.0/3) * -1 ;
									}
									
									
									B = (pow((col+row)- 2.0*glcmMean, 4)*pij)/(4.0* pow(sigma, 4)* pow(1+correlation, 2));
									if(B > 0){
										prominence += pow(abs(B), 1.0/4);
									}
									else if(B < 0){
										prominence += pow(abs(B), 1.0/4) * -1 ;
									}
								}
								
		    				}
		    			}
		    			
						//#pragma omp critical
						//{
						features = {double(row), double(col),energy, contrast, homogeneity, entropy, correlation, shade, prominence}; 
						ys.push_back(features);
						//}
		    		}
		    		std::sort(ys.begin(), ys.end());
		    		//#pragma omp critical
					//{
		    		processedFeatures.push_back(ys);
		    		ys.clear();
		    		//}
		    	}
	
		    }
		return;
        }





int main(int argc, const char* argv[]) {
    
    if (argc < 3) {
		std::cerr << "usage: ./omp-glcm <image> <window Size>\n";
		return -1;
	}
	int windowSize =0;
	try{
		windowSize = std::stoi(argv[2]);
	}
	catch(std::invalid_argument &err){
		std::cerr<<err.what()<<"\n";
	}
	
	std::string imagePath = argv[1];
	
	cv::Mat image = cv::imread(imagePath, 0);
	cv::resize(image, image, cv::Size(), 10, 10);
	
	std::cerr<<image.size<<"\n";
	cv::imwrite("resized_input.png", image);
	
	//double min, max;
	//cv::minMaxIdx(image, &min, &max);
	//std::cerr<<min <<" "<< max<<"\n";
	
	std::vector<std::vector<std::vector<double>>> processedFeatures;
	//int windowSize = 5;
	
	computeGlcm(image, windowSize, processedFeatures);
	
	int nan = 0;
	int inf = 0;
	
	std::sort(processedFeatures.begin(), processedFeatures.end());
	
	for(auto &v:processedFeatures){
		for(auto &feature: v){
			if(!isnan(feature[2]) && !isnan(feature[3]) && !isnan(feature[4]) && !isnan(feature[5]) && !isnan(feature[6]) && 
			!isnan(feature[7]) && !isnan(feature[8])){
				if(!isinf(feature[2]) && !isinf(feature[3]) && !isinf(feature[4]) && !isinf(feature[5]) && !isinf(feature[6]) && 
				!isinf(feature[7]) && !isinf(feature[8])){

					std::cout<< feature[0]<<","<< feature[1]<<","<< feature[2]<<","
					<< feature[3]<<","<< feature[4]<<","<< feature[5]<<","<< feature[6]
					<<","<< feature[7]<<","<< feature[8]<<"\n";
				}
				else{
					++inf;
				}
			}
			else{
				++nan;
			}
		}
	}
	std::cerr <<"removed "<<nan<<" features that had non valid numbers \n";
	std::cerr <<"removed "<<inf<<" features that had infinity as value \n";
	
	return 0;
	
}
