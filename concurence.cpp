#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main(int argc, char **argv)
{	
	std::string line;
	int x,y,z;
	
	std::vector<std::vector<int>> pointList;
	std::vector<int> sums;
	std::vector<int> serialSums;
	
	std::ifstream myfile ("data/example.txt");
	if (myfile.is_open()){
		while ( getline (myfile,line) ) {
			std::istringstream ss(line);
			ss >> x >> y >> z;
			std::vector<int> point{x,y,z};
			pointList.push_back(point);
		}	
	}
	
	else std::cout << "Unable to open file";
	
	myfile.close();
	#pragma omp parallel for
	for(auto &point : pointList){
		#pragma omp critical // pour eviter la concurrence
		{
		sums.push_back(point[0]+point[1]+point[2]); // concurence quand plusieurs coeur accede au vecteur
		}
	}
	
	for(auto &point : pointList){
		serialSums.push_back(point[0]+point[1]+point[2]);
	}
	
	int equal =0;
	for(int i = 0; i < serialSums.size(); i++){
		for(int j = 0; j<sums.size(); j++){
			if(serialSums[i]==sums[j]){
				sums[j]= 0;
				equal++;
			}
		}
	}
	if(equal == serialSums.size()){
		std::cout<<"equal"<<"!\n";
	}
	std::cout<<equal<<"\n";
	return 0;
}
